<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql_ui\Operation\OperationInterface;

/**
 * Defines an interface for Schema extension entities.
 */
interface SchemaExtensionInterface extends ConfigEntityInterface {

  /**
   * Gets the extension description.
   *
   * @return string
   *   Description.
   */
  public function getDescription(): string;

  /**
   * Gets query operations.
   *
   * @return \Drupal\graphql_ui\Operation\OperationInterface[]
   */
  public function getQueryOperations(): array;

  /**
   * Tests if the extension has query operations.
   *
   * @return bool
   *   TRUE if the extension has query operations.
   */
  public function hasQueryOperations(): bool;

  /**
   * Gets all operations.
   *
   * @return \Drupal\graphql_ui\Operation\OperationInterface[]
   */
  public function getAllOperations(): array;

  /**
   * Registers resolvers for the given operation.
   *
   * @param \Drupal\graphql_ui\Operation\OperationInterface $operation
   *   Operation.
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  public function registerResolversForOperation(OperationInterface $operation, ResolverRegistryInterface $registry, ResolverBuilder $builder): void;

  /**
   * Gets schema lines for the given operation.
   *
   * @param \Drupal\graphql_ui\Operation\OperationInterface $operation
   *   Operation to get schema lines for.
   *
   * @return array
   *   Schema lines - each line of the schema should be returned in order to be
   *   joined together to generate a schema.
   *   E.g. this return:
   *   @code
   *     [
   *       'type Tag {',
   *       'id: String!',
   *       'name: String!',
   *       '}',
   *     ],
   *   @endcode
   *   Would end up as this schema definition:
   *   @code
   *     type Tag {
   *       id: String!
   *       name: String!
   *       tid: Int!
   *     }
   *   @endcode
   */
  public function getSchemaForResolvers(OperationInterface $operation): array;

}
