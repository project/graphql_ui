<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql_ui\ConfigurableResolver\ConfigurableResolverInterface;
use Drupal\graphql_ui\Operation\OperationInterface;

/**
 * Defines a config entity for a GraphQL schema extension.
 *
 * @ConfigEntityType(
 *   id = "graphql_ui_schema_extension",
 *   label = @Translation("Schema extension"),
 *   label_singular = @Translation("schema extension"),
 *   label_plural = @Translation("schema extensions"),
 *   label_collection = @Translation("Schema Extensions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count schema extension",
 *     plural = "@count schema extensions"
 *   ),
 *   handlers = {
 *     "list_builder" = \Drupal\graphql_ui\EntityHandlers\SchemaExtensionListBuilder::class,
 *     "form" = {
 *       "default" = \Drupal\graphql_ui\Form\SchemaExtensionForm::class,
 *       "delete" = \Drupal\Core\Entity\EntityDeleteForm::class,
 *     },
 *     "route_provider" = {
 *       "html" = \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider::class,
 *     }
 *   },
 *   admin_permission = "administer graphql ui schema extensions",
 *   config_prefix = "schema_extension",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/graphql/schema-extensions/add",
 *     "delete-form" = "/admin/config/graphql/schema-extensions/manage/{graphql_ui_schema_extension}/delete",
 *     "edit-form" = "/admin/config/graphql/schema-extensions/manage/{graphql_ui_schema_extension}/edit",
 *     "collection" = "/admin/config/graphql/schema-extensions",
 *   },
 *   config_export = {
 *     "name",
 *     "id",
 *     "description",
 *     "operations",
 *     "resolvers",
 *   }
 * )
 */
class SchemaExtension extends ConfigEntityBase implements SchemaExtensionInterface, EntityWithPluginCollectionInterface {

  /**
   * Operations plugins.
   *
   * @var array
   */
  protected $operations = [];

  /**
   * Resolvers.
   *
   * @var array
   */
  protected $resolvers = [];

  /**
   * Holds the plugin collection for the operations plugins.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection|null
   */
  protected $operationCollection = NULL;

  /**
   * Holds the plugin collection for the resolver plugins.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection|null
   */
  protected $resolverCollection = NULL;

  /**
   * Description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'operations' => new DefaultLazyPluginCollection(\Drupal::service('plugin.manager.graphql_ui_operation'), self::pluginConfigurationWithIds($this->operations)),
      'resolvers' => new DefaultLazyPluginCollection(\Drupal::service('plugin.manager.graphql_ui_resolver'), self::pluginConfigurationWithIds($this->resolvers)),
    ];
  }

  /**
   * Adds IDs to plugin configuration.
   *
   * @param array $configuration
   *   Plugin configuration, keyed by plugin ID.
   *
   * @return array
   *   Plugin configuration, with an ID key added to each item.
   */
  protected static function pluginConfigurationWithIds(array $configuration): array {
    foreach ($configuration as $id => $item_configuration) {
      $configuration[$id]['id'] = $id;
    }
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryOperations(): array {
    return array_filter($this->getAllOperations(), function (OperationInterface $operation) {
      return $operation->isQuery();
    });
  }

  /**
   * {@inheritdoc}
   */
  public function hasQueryOperations(): bool {
    return (bool) array_filter($this->getAllOperations(), function (OperationInterface $operation) {
      return $operation->isQuery();
    });
  }

  /**
   * {@inheritdoc}
   */
  public function getAllOperations(): array {
    return iterator_to_array($this->getPluginCollections()['operations']);
  }

  /**
   * Gets all resolvers.
   *
   * @return \Drupal\graphql_ui\ConfigurableResolver\ConfigurableResolverInterface[]
   *   Resolvers.
   */
  protected function getAllResolvers(): array {
    return iterator_to_array($this->getPluginCollections()['resolvers']);
  }

  /**
   * Filters resolvers for a given operation.
   *
   * @param \Drupal\graphql_ui\Operation\OperationInterface $operation
   *   Operation to filter for.
   *
   * @return \Drupal\graphql_ui\ConfigurableResolver\ConfigurableResolverInterface[]
   *   Filtered resolvers.
   */
  protected function filterResolversForOperation(OperationInterface $operation): array {
    return array_filter($this->getAllResolvers(), function (ConfigurableResolverInterface $resolver) use ($operation) {
      return $resolver->appliesToOperation($operation);
    });
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolversForOperation(OperationInterface $operation, ResolverRegistryInterface $registry, ResolverBuilder $builder): void{
    $resolvers = $this->filterResolversForOperation($operation);
    foreach ($resolvers as $resolver) {
      assert($resolver instanceof ConfigurableResolverInterface);
      $resolver->registerResolver($operation, $registry, $builder);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaForResolvers(OperationInterface $operation): array {
    $resolvers = $this->filterResolversForOperation($operation);
    $lines = [];
    foreach ($resolvers as $resolver) {
      assert($resolver instanceof ConfigurableResolverInterface);
      $lines = array_merge($lines, $resolver->getSchemaLines($operation));
    }
    return $lines;
  }

}
