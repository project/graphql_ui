<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an annotation for GraphQL UI types.
 *
 * @Annotation
 */
class Type extends Plugin {

  /**
   * Operation ID.
   *
   * @var string
   */
  public $id;

  /**
   * Operation label.
   *
   * @var string
   */
  public $label;

  /**
   * Default name given to return.
   *
   * @var string
   */
  public $default_name;

  /**
   * Plugin name.
   *
   * @var string
   */
  public $name;

}
