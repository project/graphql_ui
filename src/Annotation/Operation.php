<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an annotation for GraphQL UI operations.
 *
 * @Annotation
 */
class Operation extends Plugin {

  /**
   * Operation ID.
   *
   * @var string
   */
  public $id;

  /**
   * TRUE if this is a Query operation.
   *
   * @var bool|null
   */
  public $query;

  /**
   * Operation label.
   *
   * @var string
   */
  public $label;

  /**
   * Default name given to operation.
   *
   * @var string
   */
  public $default_name;

  /**
   * Plugin name.
   *
   * @var string
   */
  public $name;

}
