<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an annotation for a configurable resolver plugin.
 *
 * @Annotation
 */
class ConfigurableResolver extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * An array of context definitions describing the context used by the plugin.
   *
   * The array is keyed by context names.
   *
   * @var \Drupal\Core\Annotation\ContextDefinition[]
   */
  public $context_definitions = [];

}
