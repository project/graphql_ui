<?php

namespace Drupal\graphql_ui;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\graphql\GraphQL\Buffers\EntityBuffer;
use GraphQL\Deferred;

/**
 * Provides a collection connection value object.
 */
class CollectionConnection {

  /**
   * Entity query to wrap.
   *
   * @var \Drupal\Core\Entity\Query\Sql\Query
   */
  protected $query;

  /**
   * Entity buffer.
   *
   * @var \Drupal\graphql\GraphQL\Buffers\EntityBuffer
   */
  protected $entityBuffer;

  /**
   * CollectionConnection constructor.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   Query to wrap.
   * @param \Drupal\graphql\GraphQL\Buffers\EntityBuffer $entityBuffer
   *   Buffer.
   */
  public function __construct(QueryInterface $query, EntityBuffer $entityBuffer) {
    $this->query = $query;
    $this->entityBuffer = $entityBuffer;
  }

  /**
   * Gets the collection total.
   *
   * @return int
   *   Total.
   */
  public function total() {
    $query = clone $this->query;
    $query->range(NULL, NULL)->count();
    return $query->execute();
  }

  /**
   * Gets the collection items.
   *
   * @return array|\GraphQL\Deferred
   *   Items.
   */
  public function items() {
    $result = $this->query->execute();
    if (empty($result)) {
      return [];
    }

    $callback = $this->entityBuffer->add($this->query->getEntityTypeId(), array_values($result));
    return new Deferred(function () use ($callback) {
      return $callback();
    });
  }

}
