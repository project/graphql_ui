<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Type;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\graphql\GraphQL\Resolver\ResolverInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;

/**
 * Defines an interface for schema types.
 */
interface TypePluginInterface extends ConfigurableInterface, PluginWithFormsInterface {

  public function getSchemaDefinition(): string;

  public function getName(): string;

}
