<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Type;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsTrait;

/**
 * Defines a base class for type plugins.
 */
abstract class TypePluginBase extends PluginBase implements TypePluginInterface {

  use PluginWithFormsTrait;

  /**
   * {@inheritdoc}
   */
  protected $configuration = [];

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): TypePluginInterface {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

}
