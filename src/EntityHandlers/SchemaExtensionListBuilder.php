<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\EntityHandlers;

use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a list builder for Schema extension entities.
 */
class SchemaExtensionListBuilder extends EntityListBuilder {

}
