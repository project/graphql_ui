<?php

declare(strict_types=1);

namespace Drupal\graphql_ui;

use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\Resolver\ResolverInterface;
use GraphQL\Type\Definition\ResolveInfo;

class Arguments implements ResolverInterface {

  /**
   * Fields.
   *
   * @var array
   */
  protected $arguments = [];

  /**
   * Constructs a new Arguments resolver.
   *
   * @param array $arguments
   *   Arguments to resolve.
   */
  public function __construct(array $arguments = []) {
    $this->arguments = $arguments;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve($value, $args, ResolveContext $context, ResolveInfo $info, FieldContext $field) {
    $return = [];
    foreach ($this->arguments as $field_name => $details) {
      // @todo - validation?
      $return[$field_name] = $args[$details['name']] ?? NULL;
    }
    return $return;
  }

}
