<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Operation;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\graphql\GraphQL\Resolver\ResolverInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;

/**
 * Defines an interface for schema operations.
 */
interface OperationInterface extends ConfigurableInterface, PluginWithFormsInterface {

  const TYPE_QUERY = 'query';
  const TYPE_MUTATION = 'mutation';

  public function isQuery(): bool;

  public function getSchemaDefinition(): string;

  public function getName(): string;

  public function getResolver(ResolverBuilder $builder): ResolverInterface;

  public function addFieldResolvers(ResolverRegistry $registry, ResolverBuilder $builder): void;

  public function uuid(): string;

  /**
   * Gets return type name.
   *
   * For example, in an operation like so
   *
   * @code
   * extend type Query {
   *   article(id: Int!): Article
   * }
   * @endcode
   *
   * The return type-name is 'Article'
   *
   * @return string
   *   The type definition used in the schema for the return of this operation.
   */
  public function getReturnTypeName(): string;

  public function getFieldSchema(): array;

}
