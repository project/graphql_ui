<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Operation;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\graphql_ui\Annotation\Operation;

/**
 * Defines a plugin manager for operations.
 */
class OperationManager extends DefaultPluginManager {

  /**
   * Constructs a RecipientTypePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/GraphQLUI/Operation', $namespaces, $module_handler, OperationInterface::class, Operation::class);
    $this->alterInfo('graphql_ui_operations');
    $this->setCacheBackend($cache_backend, 'graphql_ui_operations');
  }

}
