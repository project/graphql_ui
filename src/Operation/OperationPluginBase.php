<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Operation;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base plugin for an operation.
 */
abstract class OperationPluginBase extends PluginBase implements OperationInterface, ContainerFactoryPluginInterface {

  use PluginWithFormsTrait;

  /**
   * {@inheritdoc}
   */
  protected $configuration = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $escaped_plugin_id = str_replace(PluginBase::DERIVATIVE_SEPARATOR, '__', $plugin_id);
    $configuration += [
      'uuid' => $container->get('uuid')->generate(),
      'return_name' => Container::camelize($plugin_definition['default_name'] ?? $escaped_plugin_id),
      'name' => $plugin_definition['default_name'] ?? $escaped_plugin_id,
    ];
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function uuid(): string {
    return $this->configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldResolvers(ResolverRegistry $registry, ResolverBuilder $builder): void {
    // Nil-op.
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['name' => '', 'return_name' => '', 'uuid' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function getReturnTypeName(): string {
    return $this->configuration['return_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function isQuery(): bool {
    return $this->pluginDefinition['query'] === TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->configuration['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldSchema(): array {
    return [];
  }

}
