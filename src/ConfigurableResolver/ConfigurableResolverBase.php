<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\ConfigurableResolver;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\graphql_ui\Operation\OperationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base plugin class for configurable resolvers.
 */
abstract class ConfigurableResolverBase extends PluginBase implements ConfigurableResolverInterface, ContainerFactoryPluginInterface {

  use PluginWithFormsTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return ['operation_id' => '', 'property_name' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ConfigurableResolverInterface {
    if (isset($plugin_definition['default_property_name'])) {
      $configuration += [
        'property_name' => $plugin_definition['default_property_name'],
      ];
    }
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function appliesToOperation(OperationInterface $operation): bool {
    return ($this->configuration['operation_id'] ?? '') === $operation->uuid();
  }

  protected function getPropertyName(): string {
    return $this->configuration['property_name'];
  }

}
