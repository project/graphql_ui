<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\ConfigurableResolver;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql_ui\Operation\OperationInterface;

/**
 * Defines an interface for a configurable resolver.
 */
interface ConfigurableResolverInterface extends ConfigurableInterface, PluginWithFormsInterface {

  /**
   * Checks if an operation applies to this resolver.
   *
   * @param \Drupal\graphql_ui\Operation\OperationInterface $operation
   *   Operation.
   *
   * @return bool
   *   TRUE if this resolver applies.
   */
  public function appliesToOperation(OperationInterface $operation): bool;

  /**
   * Adds resolver to the registry.
   *
   * @param \Drupal\graphql_ui\Operation\OperationInterface $operation
   *   Operation.
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Builder.
   */
  public function registerResolver(OperationInterface $operation, ResolverRegistryInterface $registry, ResolverBuilder $builder): void;


  public function getSchemaLines(OperationInterface $operation): array;

}
