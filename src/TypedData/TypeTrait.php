<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\TypedData;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\TypedData\Type\BooleanInterface;
use Drupal\Core\TypedData\Type\FloatInterface;
use Drupal\Core\TypedData\Type\IntegerInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;

/**
 * Defines a trait for determining the GraphQL type of type data items.
 */
trait TypeTrait {

  /**
   * Gets the return type.
   *
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   Typed data manager.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition to get the return type for.
   * @param string|null $property_name
   *   Property name. If omitted, defaults to the main property name.
   *
   * @return string
   *   Return type.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the field uses an unknown data-type property.
   */
  protected static function getGraphQlTypeForFieldProperty(TypedDataManagerInterface $typed_data_manager, FieldDefinitionInterface $field_definition, ?string $property_name = NULL): string {
    $required = $field_definition->isRequired();
    $field_data_type = $field_definition->getItemDefinition()->getDataType();
    $data_definition = $typed_data_manager->getDefinition($field_data_type);
    $class = $data_definition['class'];
    $field_storage_definition = $field_definition->getFieldStorageDefinition();
    $propery_definitions = $class::propertyDefinitions($field_storage_definition);
    $property = $propery_definitions[$property_name ?: $field_storage_definition->getMainPropertyName()];
    $property_data_type = $typed_data_manager->getDefinition($property->getDataType());
    $property_class = $property_data_type['class'];
    if (is_a($property_class, IntegerInterface::class, TRUE)) {
      return 'Int' . ($required ? '!' : '');
    }
    if (is_a($property_class, BooleanInterface::class, TRUE)) {
      return 'Boolean' . ($required ? '!' : '');
    }
    if (is_a($property_class, FloatInterface::class, TRUE)) {
      return 'Float' . ($required ? '!' : '');
    }
    // @todo add support for enum.
    // Fallback to string.
    return 'String' . ($required ? '!' : '');
  }

}
