<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Plugin\GraphQL\Schema;

use Drupal\Core\Form\FormStateInterface;
use Drupal\graphql\Plugin\GraphQL\Schema\ComposableSchema;

/**
 * Configurable Schema.
 *
 * @Schema(
 *   id = "graphql_ui_schema",
 *   name = "Configurable schema"
 * )
 */
class ConfigurableSchema extends ComposableSchema {

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $formState): void {
    // @todo Validate that the user hasn't selected extensions that have
    //   clashing schemas.
    parent::validateConfigurationForm($form, $formState);
  }

}
