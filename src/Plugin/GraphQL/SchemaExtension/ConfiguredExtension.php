<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Plugin\GraphQL\SchemaExtension;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_ui\Entity\SchemaExtensionInterface;
use Drupal\graphql_ui\Operation\OperationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @SchemaExtension(
 *   id = "graphql_ui_schema_extension",
 *   name = "Configured schema extension",
 *   description = "A schema extension that is built from config entities.",
 *   schema = "graphql_ui_schema",
 *   deriver = \Drupal\graphql_ui\Plugin\Deriver\ConfiguredExtensionDeriver::class
 * )
 */
class ConfiguredExtension extends SdlSchemaExtensionPluginBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Gets the schema extension entity.
   *
   * @return \Drupal\graphql_ui\Entity\SchemaExtensionInterface
   *   Schema extension.
   */
  private function getSchemaExtensionEntity(): SchemaExtensionInterface {
    $schema_extension = $this->entityTypeManager->getStorage('graphql_ui_schema_extension')->load($this->pluginDefinition['entity_id']);
    if (!$schema_extension) {
      throw new PluginNotFoundException(sprintf('The %s schema extension no longer exists', $this->pluginDefinition['entity_id']));
    }
    assert($schema_extension instanceof SchemaExtensionInterface);
    return $schema_extension;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) : ConfiguredExtension {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();

    $extension = $this->getSchemaExtensionEntity();
    foreach ($extension->getQueryOperations() as $operation) {
      assert($operation instanceof OperationInterface);
      $registry->addFieldResolver('Query', $operation->getName(), $operation->getResolver($builder));
      $operation->addFieldResolvers($registry, $builder);
      $extension->registerResolversForOperation($operation, $registry, $builder);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseDefinition(): string {
    $extension = $this->getSchemaExtensionEntity();
    $type_definition_lines = [];
    foreach ($extension->getAllOperations() as $operation) {
      assert($operation instanceof OperationInterface);
      $type_definition_lines = array_merge($type_definition_lines, ["\n\n"],
        [
          sprintf('type %s {', $operation->getReturnTypeName()),
        ],
        $operation->getFieldSchema(),
        $extension->getSchemaForResolvers($operation),
        [
          '}',
        ]
      );
    }
    return implode("\n", $type_definition_lines);
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDefinition(): string {
    $extension = $this->getSchemaExtensionEntity();
    $definition_lines = [];
    if ($extension->hasQueryOperations()) {
      $definition_lines[] = 'extend type Query {';
      foreach ($extension->getQueryOperations() as $operation) {
        assert($operation instanceof OperationInterface);
        $definition_lines[] = $operation->getSchemaDefinition();
      }
      $definition_lines[] = '}';
    }
    return implode("\n", $definition_lines);
  }

}
