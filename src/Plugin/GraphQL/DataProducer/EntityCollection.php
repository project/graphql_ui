<?php

namespace Drupal\graphql_ui\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_ui\CollectionConnection;
use GraphQL\Error\UserError;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Blogs data producer.
 *
 * @DataProducer(
 *   id = "graphql_ui_collection",
 *   name = @Translation("Entity Collection"),
 *   description = @Translation("Loads a collection of entities."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Entities")
 *   ),
 *   consumes = {
 *     "entity_type_id" = @ContextDefinition("string",
 *       label = @Translation("Entity Type ID"),
 *       required = TRUE
 *     ),
 *     "bundle_id" = @ContextDefinition("string",
 *       label = @Translation("Bundle ID"),
 *       required = FALSE
 *     ),
 *     "offset" = @ContextDefinition("integer",
 *       label = @Translation("Offset"),
 *       required = FALSE
 *     ),
 *     "limit" = @ContextDefinition("integer",
 *       label = @Translation("Limit"),
 *       required = FALSE
 *     ),
 *     "filters" = @ContextDefinition("any",
 *       label = @Translation("Filters"),
 *       required = FALSE
 *     ),
 *   }
 * )
 */
class EntityCollection extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  const MAX_LIMIT = 100;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAlias;

  /**
   * Entity buffer.
   *
   * @var \Drupal\graphql\GraphQL\Buffers\EntityBuffer
   */
  protected $entityBuffer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): EntityCollection {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentUser = $container->get('current_user');
    $instance->pathAlias = $container->get('path_alias.manager');
    $instance->entityBuffer = $container->get('graphql.buffer.entity');
    return $instance;
  }

  /**
   * Resolves the data.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $bundle_id
   *   Bundle ID.
   * @param int $offset
   *   Offset.
   * @param int $limit
   *   Limit.
   * @param array $filters
   *   Query conditions.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache data.
   *
   * @return \Drupal\graphql_ui\CollectionConnection
   *   Query result value object.
   */
  public function resolve(string $entity_type_id, string $bundle_id, ?int $offset, ?int $limit, ?array $filters, RefinableCacheableDependencyInterface $metadata): CollectionConnection {
    if ($limit > static::MAX_LIMIT) {
      throw new UserError(sprintf('Exceeded maximum query limit: %s.', static::MAX_LIMIT));
    }

    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $type = $storage->getEntityType();
    $query = $storage->getQuery()
      ->currentRevision()
      ->accessCheck();

    if (!empty($bundle_id) && $bundle_key = $type->getKey('bundle')) {
      $query->condition($bundle_key, $bundle_id);
    }

    if (isset($filters['path']) && $alias = $this->pathAlias->getPathByAlias($filters['path'])) {
      $query->condition('nid', basename($alias));
      unset($filters['path']);
    }

    foreach ($filters as $field_name => $value) {
      $query->condition($field_name, $value);
    }

    $metadata->addCacheContexts(['user.permissions']);

    // @todo use entity reference selection handlers here for things like
    //   permissions that bypass access - or add a new plugin system 🤔?

    $query->range((int) $offset, (int) $limit);

    $metadata->addCacheTags($type->getListCacheTags());
    $metadata->addCacheContexts($type->getListCacheContexts());

    return new CollectionConnection($query, $this->entityBuffer);
  }

}
