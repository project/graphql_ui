<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Plugin\GraphQLUI\Type;

use Drupal\graphql_ui\Type\TypePluginBase;

/**
 * Defines a plugin for a Type based off an entity.
 *
 * @Type(
 *   id="entity",
 *   name="Content entity",
 *   deriver=\Drupal\graphql_ui\Plugin\Deriver\PerContentEntityTypeOperationDeriver::class
 * )
 */
class ContentEntity extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function getSchemaDefinition(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string{
    return $this->configuration['name'];
  }

}
