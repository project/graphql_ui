<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Plugin\GraphQLUI\Operation;

use Drupal\graphql\GraphQL\Resolver\ResolverInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql_ui\Annotation\Operation;
use Drupal\graphql_ui\Operation\OperationPluginBase;

/**
 * Defines a plugin to load an entity by UUID.
 *
 * @Operation(
 *   id="entity_by_uuid",
 *   query=TRUE,
 *   name="By UUID",
 *   deriver=\Drupal\graphql_ui\Plugin\Deriver\PerContentEntityTypeOperationDeriver::class
 * )
 */
class EntityByUuid extends OperationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getSchemaDefinition(): string {
    return sprintf('%s(id: String!): %s', $this->getName(), $this->getReturnTypeName());
  }

  public function getFieldSchema(): array {
    return [
      'id: String!',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getResolver(ResolverBuilder $builder): ResolverInterface{
    return $builder->produce('entity_load_by_uuid')
      ->map('type', $builder->fromValue($this->pluginDefinition['entity_type_id']))
      ->map('uuid', $builder->fromArgument('id'));
  }

}
