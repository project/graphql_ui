<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Plugin\GraphQLUI\Operation;

use Drupal\Core\Plugin\PluginBase;
use Drupal\graphql\GraphQL\Resolver\ResolverInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql_ui\Annotation\Operation;
use Drupal\graphql_ui\CollectionConnection;
use Drupal\graphql_ui\Arguments;
use Drupal\graphql_ui\Operation\OperationPluginBase;
use Drupal\graphql_ui\TypedData\TypeTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a plugin to load a collection of entities.
 *
 * @Operation(
 *   id="entity_bundle_collection",
 *   query=TRUE,
 *   name="Collection",
 *   deriver=\Drupal\graphql_ui\Plugin\Deriver\PerContentEntityTypeAndBundleOperationDeriver::class
 * )
 *
 * @todo this needs to be used to derive a data producer that returns a buffer.
 */
class EntityBundleCollection extends OperationPluginBase {

  use TypeTrait;

  protected $entityTypeId;
  protected $bundleId;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    [, $this->entityTypeId, $this->bundleId] = explode(PluginBase::DERIVATIVE_SEPARATOR, $plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): EntityBundleCollection {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->typedDataManager = $container->get('typed_data_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'arguments' => [],
      'item_type' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaDefinition(): string {
    return sprintf('%s(%s): %s!', $this->getName(), $this->getArguments(), $this->getReturnTypeName());
  }

  protected function getArguments(): string {
    $arguments = [
      'limit: Int!',
      'offset: Int!',
    ];
    foreach ($this->configuration['arguments'] as $field_name => $details) {
      $field_definition = $this->entityFieldManager->getFieldDefinitions($this->entityTypeId, $this->bundleId)[$field_name] ?? NULL;
      if (!$field_definition) {
        // @todo Log this.
        continue;
      }
      $arguments[] = sprintf('%s: %s%s', $details['name'], self::getGraphQlTypeForFieldProperty($this->typedDataManager, $field_definition), ($details['required'] ?? FALSE) ? '!' : '');
    }
    return implode(', ', $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldResolvers(ResolverRegistry $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver($this->getReturnTypeName(), 'total',
      $builder->callback(function (CollectionConnection $connection) {
        return $connection->total();
      })
    );
    $registry->addFieldResolver($this->getReturnTypeName(), 'items',
      $builder->callback(function (CollectionConnection $connection) {
        return $connection->items();
      })
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldSchema(): array {
    return parent::getFieldSchema() + [
      'total: Int!',
      // @todo this needs to be configurable. Add a plugin definition key for
      // 'defines_type' vs 'needs_type'. Don't add fields if 'needs_type'.
      sprintf('items: [%s]!', $this->configuration['item_type']),
     ];
  }

  /**
   * {@inheritdoc}
   */
  public function getResolver(ResolverBuilder $builder): ResolverInterface {
    $resolver = $builder->produce('graphql_ui_collection')
      ->map('entity_type_id', $builder->fromValue($this->pluginDefinition['entity_type_id']))
      ->map('bundle_id', $builder->fromValue($this->pluginDefinition['bundle_id']))
      ->map('offset', $builder->fromArgument('offset'))
      ->map('limit', $builder->fromArgument('limit'))
      ->map('filters', new Arguments($this->configuration['arguments']));
    return $resolver;
  }

}
