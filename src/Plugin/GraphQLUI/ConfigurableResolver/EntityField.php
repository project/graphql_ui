<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Plugin\GraphQLUI\ConfigurableResolver;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql_ui\Annotation\ConfigurableResolver;
use Drupal\graphql_ui\ConfigurableResolver\ConfigurableResolverBase;
use Drupal\graphql_ui\ConfigurableResolver\ConfigurableResolverInterface;
use Drupal\graphql_ui\Operation\OperationInterface;
use Drupal\graphql_ui\TypedData\TypeTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a configurable resolver that can resolve an entity field.
 *
 * @ConfigurableResolver(
 *   id="entity_field",
 *   deriver=\Drupal\graphql_ui\Plugin\Deriver\EntityFieldDeriver::class
 * )
 */
class EntityField extends ConfigurableResolverBase {

  use TypeTrait;

  protected $entityTypeId;
  protected $bundle;
  protected $fieldName;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    [, $this->entityTypeId, $this->bundle, $this->fieldName] = explode(PluginBase::DERIVATIVE_SEPARATOR, $plugin_id);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ConfigurableResolverInterface {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->typedDataManager = $container->get('typed_data_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'properties' => [],
      'hoist' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolver(OperationInterface $operation, ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    $field_definition = $this->getFieldDefinition();
    // @todo multi-value to single-value support
    // @todo hoisting
    // @todo specify properties
    $registry->addFieldResolver($operation->getReturnTypeName(), $this->getPropertyName(),
      $builder->produce('property_path')
        ->map('type', $builder->fromValue(sprintf('entity:%s:%s', $this->entityTypeId, $this->bundle)))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue(sprintf('%s.%s', $this->fieldName, $field_definition->getFieldStorageDefinition()->getMainPropertyName())))
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaLines(OperationInterface $operation): array {
    return [
      sprintf('%s: %s', $this->getPropertyName(), self::getGraphQlTypeForFieldProperty($this->typedDataManager, $this->getFieldDefinition())),
    ];
  }

  /**
   * Gets the field definition for this plugin.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   Field definition.
   */
  protected function getFieldDefinition(): FieldDefinitionInterface {
    $field_definition = $this->entityFieldManager->getFieldDefinitions($this->entityTypeId, $this->bundle)[$this->fieldName];
    assert($field_definition instanceof FieldDefinitionInterface);
    return $field_definition;
  }

}
