<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;

/**
 * Defines a deriver that provides one entry per content entity-type.
 */
class PerContentEntityTypeOperationDeriver extends DeriverBase implements ContainerDeriverInterface {

  use EntityTypeManagerAwareDeriverTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    foreach ($this->entityTypeManager->getDefinitions() as $id => $definition) {
      assert($definition instanceof EntityTypeInterface);
      if (!$definition->entityClassImplements(ContentEntityInterface::class)) {
        continue;
      }
      $this->derivatives[$id] = [
        'name' => sprintf('%s - %s', $base_plugin_definition['name'], $definition->getLabel()),
        'entity_type_id' => $id,
        'default_name' => $id,
      ] + $base_plugin_definition;
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
