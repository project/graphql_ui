<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\graphql_ui\Entity\SchemaExtensionInterface;

/**
 * Defines a plugin deriver for schema extension plugins.
 */
class ConfiguredExtensionDeriver extends DeriverBase implements ContainerDeriverInterface {

  use EntityTypeManagerAwareDeriverTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    foreach ($this->entityTypeManager->getStorage('graphql_ui_schema_extension')->loadMultiple() as $id => $extension) {
      assert($extension instanceof SchemaExtensionInterface);
      $this->derivatives[$id] = [
        'name' => $extension->label(),
        'description' => $extension->getDescription(),
        'entity_id' => $extension->id(),
      ] + $base_plugin_definition;
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
