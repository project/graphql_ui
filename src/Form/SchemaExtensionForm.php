<?php

declare(strict_types=1);

namespace Drupal\graphql_ui\Form;

use Drupal\Core\Entity\EntityForm;

/**
 * Defines a form for editing Schema extension entities.
 */
class SchemaExtensionForm extends EntityForm {

}
